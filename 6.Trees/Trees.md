# Tree Data Structures

A tree is a widely used abstract data type (ADT) that simulates a hierarchical tree structure, with a root value and subtrees of children with a parent node, represented as a set of linked nodes.

A tree data structure can be defined recursively as a collection of nodes (starting at a root node), where each node is a data structure consisting of a value, together with a list of references to nodes (the "children"), with the constrain ts that no reference is duplicated, and none points to the root.

Alternatively, a tree can be defined abstractly as a whole (globally) as an ordered tree, with a value assigned to each node. Both these perspectives are useful: while a tree can be analyzed mathematically as a whole, when actually represented as a data structure it is usually represented and worked with separately by node (rather than as a set of nodes and an adjacency list of edges between nodes, as one may represent a digraph, for instance). For example, looking at a tree as a whole, one can talk about "the parent node" of a given node, but in general as a data structure a given node only contains the list of its children, but does not contain a reference to its parent (if any).

[Wikipedia](https://en.wikipedia.org/wiki/Tree_%28data_structure%29)

## Tree Structure
* Root
* Parent
* Child
* Siblings
* Descendants
* Ancestors
* Internal Nodes e(Non-Leaf nodes)
* External Nodes (Leaf nodes)

* Degree of Node:
  The number of children of the node
  Degree of leaf node is Zero

* Degree of Tree:
  The maximum of nodes degree.

* Levels & Heights:
  Root at level 1
  Root at height 0

* Subtree
  Any node can be considered as a Sub-tree

* Edge
  Define the relationship between nodes

* Path
  The relation from root to leaf-node

* Forest
  A collection of trees


## Binary Trees:
A special kind of tree, with two children at most
- Every node has at most two children
- Every child nodes is labelled as left child and right child
- Left child precedes right child in order of Nodes.
- Binary Tree degree will at max be 2.

### Properties:
1. Every binary tree with n Nodes has exactly n-1 edges
2. Full binary tree(proper): if each node has either two or zero children.
3. Complete Binary Tree: a Binary tree where nodes at each level are numbered from left to right without any gap. (works for missing right child, order is most important)
4. Perfect Binary tree: a Binary Tree where all non-leaf nodes have two children and all leaf nodes are at same level.

### Binary Trees - Level Order Traversal
Nodes are visited by Level from top to bottom and within levels, Nodes are visited from left to right.

### Binary Trees - Pre-Order Traversal
Root is visited first followed by left Sub-tree and then the Right Sub-tree

### Binary Trees - In-Order Traversal
Left Sub-Tree is visited first, followed by Root and then Right Sub-tree

### Binary Trees - Post-Order Traversal
Left Sub-Tree is visited first, followed by Right Sub-Tree and then
