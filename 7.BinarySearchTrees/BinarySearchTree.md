# Binary Search Trees
A Special form of Binary Trees
* Every node has a key
* Keys in Left Sub-Tree of Root are smaller than the key in the Root
* Keys in Right Sub-Tree of Root are larger than the key in the Root
* Left and Right Sub-Trees are also Binary Search Trees

If Smaller than Root, go Left; Else, go Right!
