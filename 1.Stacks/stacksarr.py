from exceptions import Empty

class ArrayStack:
    def __init__(self):
        self._data = []

    def __len(self):
        return len(self._data)

    def is_empty(self):
        return len(self._data) == 0

    def push(self, e):
        self._data.append(e)

    def pop(self):
        if self.is_empty():
            raise Empty("Stack is Empty")
        return self._data.pop()

    def len(self):
        return len(self._data)

    def top(self):
        if self.is_empty():
            raise Empty("Stack is Empty")
        return self._data[-1]


s = ArrayStack()
s.push(10)
s.push(20)
print('Stack: ', s._data)
print('Length: ', len(s._data))
print('Length: ', s.len())
print('Is empty: ', s.is_empty())
print('Popped: ', s.pop())
print('Is empty: ', s.is_empty())
print('Stack: ', s._data)
print( 'Top element: ', s.top() )
# print( s.__len() )
# print( s.__len )
