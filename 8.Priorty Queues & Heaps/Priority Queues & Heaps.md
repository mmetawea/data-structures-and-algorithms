# Queues

* Queues collection of objects follows FIFO
  Example: customers waiting for service

* Many real applications FIFO policy does not suit
  Example: Air traffic control

* Though FIFO policy is reasonable, but some situations require objects to be prioritized.
* This is achieved using Priority Queues

## Priority Queues
* Collection of prioritized Objects or Elements.
* Allows element insertion
* Removal of element is based on Priority
* A key is associated when element is inserted in the priority queue.
* Element with minimum key will be next element to be removed.

## Heaps
* Heap is an efficient realization of Priority Queue
* Heap is a binary tree
* Relational Property:
  * In a heap the value in each node is greater than or equal to those in its children.
* Structural Property:
  * Heap is a complete binary tree
* Max Heap and Min Heap
* Conditions:
  1. Root > Left, Root > Right
  2. Complete Binary Tree (Left < Root < Right)

## Heap Abstract Data Types
* Heap ADT stores prioritized Objects
* Members:
  * MaxSize
  * CurrentSize
  * Heaplist

* Operations:
  * insert(object): insert element
  * deleteMax(): remove return element
  * max(): return root element
  * len():  returns number of Elements
  * isEmpty(): whether stack is empty or not
