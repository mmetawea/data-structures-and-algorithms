"""
Heaps are binary trees for which every parent node has a value less than or equal to any of its children. This implementation uses arrays for which heap[k] <= heap[2*k+1] and heap[k] <= heap[2*k+2] for all k, counting elements from zero. For the sake of comparison, non-existing elements are considered to be infinite. The interesting property of a heap is that its smallest element is always the root, heap[0].

The API below differs from textbook heap algorithms in two aspects: (a) We use zero-based indexing. This makes the relationship between the index for a node and the indexes for its children slightly less obvious, but is more suitable since Python uses zero-based indexing. (b) Our pop method returns the smallest item, not the largest (called a “min heap” in textbooks; a “max heap” is more common in texts because of its suitability for in-place sorting).

Args:
    variable (type): description

Returns:
    type: description

Raises:
    Exception: description

"""
import heapq as heap

L = []
heap.heappush(L, 20)
heap.heappush(L, 14)
heap.heappush(L, 5)
heap.heappush(L, 15)
heap.heappush(L, 10)
heap.heappush(L, 2)
# heap.heappush(L, 45)
# heap.heappush(L, 30)
# heap.heappush(L, 35)

print(L)
print(heap.heappop(L))
print(L)
print(heap.heappushpop(L, 18))
print(L)

L1 = heap.nlargest(3, L)
print(L1)
L2 = heap.nsmallest(3, L)
print(L2)
L3 = [20, 14, 2, 15, 10, 21]
print(L3)
heap.heapify(L3)
print(L3)

# help(heap)
