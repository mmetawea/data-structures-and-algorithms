'''
Depth-First Search (BFS)
* Start from a vertex and move to next connected Vertex
* repeat till we reached the end.
* Double back on visited nodes, ensure no connected Not-Visited
* keep going up till there are no more Not-Visited
* Protocol terminates.

1. Start at vertex 0
2. Check if nect connected edge is visited
3. If not, Visit next connected Edge
4. repeat 2 till we have no other edges.
5. recycle back checking for new not-visited Vertices.
6. Repeat, till No Not-Visited Vertices exist.
7. End
'''
'''
Breadth-First Search (BFS)
* Start from a vertex and identify all vertices reachable from it
* Then start from the first reachable vertex, identifying all its reachable vertices, if the vertex is not yet visited.
* Continue untial all vertices are visited

1. Start at vertex 0
2. Queue connected vertices
3. Dequeue & mark visited
'''

import numpy as np
from LinkedQueues import LinkedStack as LinkedQueue

class Graph:
    def __init__(self, vertices):
        self._adjMat = np.zeros((vertices, vertices))
        self._vertices = vertices
        self._visited = [0] * vertices

    def insert_edges(self, u, v, w=1):
        self._adjMat[u][v] = w

    def delete_edge(self, u, v):
        self._adjMat[u][v] = 0

    def get_edge(self, u, v):
        return self_adjMat[u][v]

    def vertices_count(self):
        return self._vertices

    def edges_count(self):
        count = 0
        for i in range(self._vertices):
            for j in range(self._vertices):
                if not self._adjMat[i][j] == 0:
                    count += 1
        return count

    def indegree(self, u):
        count = 0
        for i in range(self._vertices):
            if not self._adjMat[i][u] == 0:
                count += 1
        return count

    def outdegree(self, u):
        count = 0
        for i in range(self._vertices):
            if not self._adjMat[u][i] == 0:
                count += 1
        return count

    def display(self):
        print(self._adjMat)

    def DFS(self, source):
        if self._visited[source] == 0:
            print(source, end = ' ~ ')
            self._visited[source] = 1
            for j in range(self._vertices):
                if self._adjMat[source][j] == 1 and self._visited[j] == 0:
                    self.DFS(j)


# np.zeros((1,1))

G = Graph(7)
print('Graph Adjancency Matrix')
# G.display()
G.insert_edges(0, 1)
# print()
# G.display()
G.insert_edges(0, 5)
G.insert_edges(0, 6)
G.insert_edges(1, 0)
G.insert_edges(1, 2)
G.insert_edges(1, 5)
G.insert_edges(2, 3)
G.insert_edges(2, 4)
G.insert_edges(2, 6)
G.insert_edges(3, 4)
G.insert_edges(4, 2)
G.insert_edges(4, 5)
G.insert_edges(5, 2)
G.insert_edges(5, 3)
G.insert_edges(6, 3)
print('Graph Adjancency Matrix')
G.display()
print('Number of Vertices: ', G.vertices_count())
print('Number of Edges: ', G.edges_count())
print('Outdegree of Vertex 2: ', G.outdegree(2))
print('Indegree of Vertex 2: ', G.indegree(2))

G.DFS(0)
