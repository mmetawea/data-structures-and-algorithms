# Graphs
* Graph is a collection of object called as Vertics and together with relationship between them called as Edges
* Each edge in the Graph joins two Vertices

## Graph types:
1. Directed
2. Undirected
3. Weighted
4. Weighted Directed
5. Weighted Undirected

## Graph ADT
* Create(n)
* Add(u, v)
* Delete(u, v)
* Edges()
* Vertices()
* Exist(u, v)
* Degree(u)
* InDegree(u)
* OutDegree(u)
