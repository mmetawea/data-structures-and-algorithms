from exceptions import Empty

class CircularLinkedList:

    class _Node:
        __slots__ = '_element', '_next'

        def __init__(self, element, next):
            self._element = element
            self._next = next

    def __init__(self):
        # The Linkedlist, initially empty
        self._head = None
        self._tail = None
        self._size = 0

    def __len__(self):
        return self._size

    def is_empty(self):
        return self._size == 0

    def add_first(self, e):
        newest = self._Node(e, None)
        if self.is_empty():
            self._head = newest
            self._tail = newest
            newest._next = newest
        else:
            self._tail._next = newest
            newest._next = self._head
        self._head = newest
        self._size += 1

    def add_last(self, e):
        newest = self._Node(e, None)
        if self.is_empty():
            self._head = newest
            self._tail = newest
            newest._next = newest
        else:
            newest._next = self._tail._next
            self._tail._next = newest
        self._tail = newest
        self._size += 1

    def add_any(self, e, pos):
        newest = self._Node(e, None)
        thead = self._head
        i = 1
        while i < pos:
            thead = thead._next
            i += 1
        newest._next = thead._next
        thead._next = newest
        self._size += 1

    def remove_first(self):
        if self.is_empty():
            raise Empty("Linked List empty")
        oldhead = self._tail._next
        self._tail._next = oldhead._next
        self._head = oldhead._next

        self._size -= 1
        if self.is_empty():
            self._tail = None
        return oldhead._element

    def remove_last(self):
        if self.is_empty():
            raise Empty("Linked List empty")
        thead = self._head
        i = 0
        while i < len(self) - 2:
            thead = thead._next
            i += 1
        self._tail = thead
        thead = thead._next
        self._tail._next = self._head._next
        value = thead._element
        self._size -= 1
        return value

    def remove_any(self, pos):
        if self.is_empty():
            raise Empty("Linked List empty")
        thead = self._head
        i = 1
        while i < pos - 1:
            thead = thead._next
            i += 1
        value = thead._next._element
        thead._next = thead._next._next
        self._size -= 1
        return value

    def display(self):
        thead = self._head
        # while thead:
        #     print(thead._element, end = '->')
        #     thead = thead._next

        for i in range(self._size ):
            print(thead._element, end = '->')
            thead = thead._next
        print()

# Prog
L = CircularLinkedList()
print('adding last: 10')
L.add_last(10)
print('adding last: 20')
L.add_last(20)
print('Is empty? ', L.is_empty())
print('adding last: 30')
L.add_last(30)
print('adding first: 1')
L.add_first(1)
L.display()
print(L.is_empty())
print('adding last: 40')
L.add_last(40)
L.display()
print('Deleted first: ', L.remove_first())
L.display()
print('adding first: 70')
L.add_first(70)
L.display()
print('Deleted last: ',  L.remove_last())
L.display()
print('adding any in 2nd')
L.add_any(100, 2)
L.display()
print('removing any from 2')
L.remove_any( 2)
L.display()
