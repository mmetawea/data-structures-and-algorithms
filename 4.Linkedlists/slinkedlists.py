"""
Implementing a Singly Linked List in Python

In its most basic form, a linked list is a string of nodes, sort of like a string of pearls, with each node containing both data and a reference to the next node in the list (Note: This is a singly linked list. The nodes in a doubly linked list will contain references to both the next node and the previous node).

Singly list with head only indictor

"""
class Node:
    def __init__(self, data=None):
        self.data = data
        self.next = None

class SLinkedList:
    def __init__(self):
        self.head = None

    def Atbegining(self, data_in):
        NewNode = Node(data_in)
        NewNode.next = self.head
        self.head = NewNode

# Function to remove node
    def RemoveNode(self, Removekey):

        HeadVal = self.head

        if (HeadVal is not None):
            if (HeadVal.data == Removekey):
                self.head = HeadVal.next
                HeadVal = None
                return

        while (HeadVal is not None):
            if HeadVal.data == Removekey:
                break
            prev = HeadVal
            HeadVal = HeadVal.next

        if (HeadVal == None):
            return

        prev.next = HeadVal.next

        HeadVal = None

    def LListprint(self):
        printval = self.head
        while (printval):
            print(printval.data, end='->'),
            printval = printval.next
        print()


llist = SLinkedList()
print('Adding first')
llist.Atbegining("Thu")
print('Adding first: Wed')
llist.Atbegining("Wed")
print('Adding first: Tue')
llist.Atbegining("Tue")
print('Adding first: Mon ')
llist.Atbegining("Mon")
llist.LListprint()
print('Removing Node: Thu')
llist.RemoveNode('Thu')
llist.LListprint()
