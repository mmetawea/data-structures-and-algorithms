# Problems with Arrays
* Array is of fixed sie - immutable
* During runtime list size will be decided, we do know know in advance how many elements will be in compile time.
* we need a data structure which should grow and shrink at runtime (ie. add nodes and delete nodes)

# Linked lists
* A memory location with Value and pointer to next element.
* uses memory more efficiently
* more flexible.

```
class _Node:
  __slots__ = '_element', '_next'

  def __init__(self, element, next):
    self._element = element
    self._next = next

n1 = _Node(10, None)
n2 = _Node(20, None)
n1._next = n2
```

## Implementation

each node consists of [element, next]
each list consists of [head, tail]

A Note about classes, methods & name mangling
```
>>> class MyClass():
...     def __init__(self):
...             self.__superprivate = "Hello"
...             self._semiprivate = ", world!"
...
>>> mc = MyClass()
>>> print mc.__superprivate
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: myClass instance has no attribute '__superprivate'
>>> print mc._semiprivate
, world!
>>> print mc.__dict__
{'_MyClass__superprivate': 'Hello', '_semiprivate': ', world!'}
```
