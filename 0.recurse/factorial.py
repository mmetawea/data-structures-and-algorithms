def factorial(number):
    """
    A standard recursive factorial function in Python:

    """

    if number == 1:
        # BASE CASE
        return 1
    else:
        # RECURSIVE CASE
        # Note that `number *` happens *after* the recursive call.
        # This means that this is *not* tail call recursion.
        return number * factorial(number - 1)

def factorial2(number, accumulator=1):
    """And this is a tail call recursive version of the factorial function:"""

    if number == 0:
        # BASE CASE
        return accumulator
    else:
        # RECURSIVE CASE
        # There's no code after the recursive call.
        # This is tail call recursion:
        return factorial2(number - 1, number * accumulator)

# print(factorial(5))

%timeit factorial(10)
# 1.41 µs ± 236 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)
%timeit factorial2(10)
#1.67 µs ± 129 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)
