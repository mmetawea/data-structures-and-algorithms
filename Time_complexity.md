# Time complexity

The time complexity is the computational complexity that describes the amount of time it takes to run an algorithm. Time complexity is commonly estimated by counting the number of elementary operations performed by the algorithm, supposing that each elementary operation takes a fixed amount of time to perform. Thus, the amount of time taken and the number of elementary operations performed by the algorithm are taken to differ by at most a constant factor.

[Wikipedia](https://en.wikipedia.org/wiki/Time_complexity)
[Time Complexity Cheatsheet](https://www.bigocheatsheet.com/)
