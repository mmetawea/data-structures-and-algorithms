#!/usr/bin/env python
"""
Implementing Inseration Sort


Args:
    arr: unsorted array

Returns:
    sorts the arr in place


"""


def selectionsort(arr):
    for i in range(len(arr)-1, 0, -1):
        max_position = 0
        for j in range(1, i+1):
            if arr[j] > arr[max_position]:
                max_position = j
        arr[i], arr[max_position] = arr[max_position], arr[i]
    return arr

def selectionsort2(arr):
    for i in range(len(arr) -1):
        min_idx = i
        for j in range(i, len(arr)):
            if arr[j] < arr[min_idx]:
                min_idx = j
        arr[i], arr[min_idx] = arr[min_idx], arr[i]
    return arr

A = [84, 21, 96, 15, 47 ]
print('Original Array: ', A)
print('Sorted Array: ', selectionsort2(A))
