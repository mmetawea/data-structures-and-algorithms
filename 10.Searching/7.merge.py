'''
Merge sort
The idea is to sort smaller arrays and then combine those arrays
together (merge them) in sorted order.
Leverages recursion.

- sort the left half of the array ( assuming n > 1)
- sort the right half of the array (assuming n > 1)
- merge the two halves together.

Merge Sort – Big Oh Analysis

    Merge sort does `log n`  merge steps because each merge step double the list size.
    It does `n` work for each merge step because it must look at every item.
    So it runs on O(n log n).

Merits Of Merge Sort

    Merge sort can be applied to any file size.
    Merge Sort is useful for sorting linked lists.
    It is always fast even in worst case, its runtime is O(n log n).
    It is stable sort which means that the same element in an array maintain their original positions with respect to each other.

Demerits Of Merge Sort

    It requires additional memory to sort the elements.
    Recursive calls result in additional overhead making it unsuitable for small number of elements.

'''


def mergeSort(arr):

    # array of 1 is already sorted
    if (len(arr) < 2):
        return arr

    mid = len(arr) // 2

    left = mergeSort(arr[:mid])
    right = mergeSort(arr[mid:])

    return merge(left, right)


def merge(left, right):

    i, j = 0, 0
    result = []  # final result array

    while (i < len(left) and j < len(right)):
        if left[i] < right[j]:
            result.append(left[i])
            i += 1
        else:
            result.append(right[j])
            j += 1

    # it is basically specifies that if any element is remaining
    # in the left array from ith to the last index so that
    # it should appended into the resultant array. And similar -
    # to the right array.
    result += left[i:]
    result += right[j:]
    return result


# array to be sorted
Array = [99, 21, 19, 22, 28, 11, 14, 18]

# calling mergesort
sorted = mergeSort(Array)

print(Array)
print(sorted)
